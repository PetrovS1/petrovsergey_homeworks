package hw08;

public class Human {
    private String name;
    private int weight;

    public String getName() {
        return this.name;
    }

    public int getWeight() {
        return this.weight;
    }

    public void setWeight(int weight) {
        if (weight < 0 || weight > 350){
            weight = 0;

        }
        this.weight = weight;
    }

    public void setName(String name) {
        this.name = name;
    }
}

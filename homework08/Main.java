package hw08;

import java.util.Scanner;

//На вход подается информация о людях в количестве 10 человек
// (имя - строка, вес - вещественное число).
//Считать эти данные в массив объектов.
//Вывести в отсортированном по возрастанию веса порядке.
public class Main {
    public static void main(String[] args) {
        Human[] humans = sortHumans(getHumans(10));

        for (int i = 0; i < humans.length; i++){
            System.out.println("Человек под номером " + (i+1) + ". Его зовут: " + humans[i].getName() + ". Он весит: " + humans[i].getWeight());
        }
    }

    public static Human[] getHumans(int size) {
        Scanner scanner = new Scanner(System.in);
        Human[] humans = new Human[size];

        for (int i = 0; i < humans.length; i++) {
            int num = i + 1;
            System.out.print("Введите имя и вес(через пробел) человека № " + num + ": ");
            humans[i] = new Human();
            humans[i].setName(scanner.next());
            humans[i].setWeight(scanner.nextInt());
        }
        return humans;
    }

    public static Human[] sortHumans(Human[] humans) {
        for (int i = 0; i < humans.length; i++) {
            Human humanMinWeight = humans[i];
            int indexMinWeight = i;

            for (int j = i + 1; j < humans.length; j++) {
                if (humans[j].getWeight() < humanMinWeight.getWeight()) {
                    humanMinWeight = humans[j];
                    indexMinWeight = j;
                }
            }
            Human tmpHuman = humans[i];
            humans[indexMinWeight] = tmpHuman;
            humans[i] = humanMinWeight;
        }
        return humans;
    }
}



import java.util.Arrays;

public class Program {
    public static void main(String[] args) {
        int[] a = {24, 424, 543, 123, 22, 44};
        System.out.println(indexArr(a, 44));
        int[] b = {1, 0, 0, 4, 25, 0, 3};
        System.out.println(Arrays.toString(nullToRight(b)));
        System.out.println(Arrays.toString(nullToRight2(b)));


    }

    /*     Реализовать функцию, принимающую на вход массив и целое число.
    Данная функция должна вернуть индекс этого числа в массиве.
    Если число в массиве отсутствует - вернуть -1.*/
    public static int indexArr(int[] arr, int digit) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == digit) {
                return i;
            }
        }
        return -1;
    }

    /* Реализовать процедуру, которая переместит все значимые элементы влево, заполнив нулевые, например:
    было:
    34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20
    стало
    34, 14, 15, 18, 1, 20, 0, 0, 0, 0, 0 */
    public static int[] nullToRight(int[] arr) {
        int[] arr2 = new int[arr.length];
        for (int i = 0; i < arr.length; i++) {
            arr2[i] = 0;
        }
        int k = 0;
        for (int j = 0; j < arr.length; j++) {
            if (arr[j] != 0) {
                arr2[k] = arr[j];
                k++;
            }
        }
        return arr2;
    }

    public static int[] nullToRight2(int[] arr) {
        int k = 0;
        for (int j = 0; j < arr.length; j++) {
            if (arr[j] != 0) {
                arr[k] = arr[j];
                k++;
            }
        }
        for (int i = k; i < arr.length; i++) {
            arr[i] = 0;
        }
        return arr;
    }

}

package attestation01_oop;

public interface UserRepository {
    User findById(int id);
    void update(User user);
}

package attestation01_oop;

public class Main {
    public static void main(String[] args) {
        UserRepositoryFileImpl userRepository = new UserRepositoryFileImpl("Attestation01.txt");
        //Вывод всех текущих пользователей из файла
        System.out.println(userRepository.getAllUsers());

        //Поиск пользователя с индексом 1
        System.out.println(userRepository.findById(1));

        //Оновляем текущего пользователя 1
        User userForUpdate = userRepository.findById(1);
        userForUpdate.setName("Marsel");
        userForUpdate.setAge(27);
        userRepository.update(userForUpdate);

        //Проверяем
        System.out.println(userRepository.getAllUsers());
    }
}

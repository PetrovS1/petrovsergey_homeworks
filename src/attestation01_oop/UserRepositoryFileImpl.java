package attestation01_oop;

import java.io.*;
import java.util.*;

public class UserRepositoryFileImpl implements UserRepository {

    private final String fileName;

    public UserRepositoryFileImpl(String fileName) {
        this.fileName = fileName;
    }

    public void addNewUser(int userId, String name, int age, boolean isWorker) {
        User user = new User();
        user.setUserId(userId);
        user.setName(name);
        user.setAge(age);
        user.setWorker(isWorker);
        save(user);

    }

    public void save(User user) {
        try (Writer writer = new FileWriter(fileName, true);
             BufferedWriter bufferedWriter = new BufferedWriter(writer)) {
            bufferedWriter.write(user.getUserId() + "|" + user.getName() + "|" + user.getAge() + "|" + user.getIsWorker());
            bufferedWriter.newLine();
            bufferedWriter.flush();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private Map<Integer, User> getUsersFromFile() {
        String inputFileName = fileName;
        Map<Integer, User> usersMap = new HashMap<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(inputFileName))) {
            reader.lines().forEach(line -> {
                User userFromTxt = new User(line);
                usersMap.put(userFromTxt.getUserId(), userFromTxt);
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
        return usersMap;
    }

    @Override
    public User findById(int id) {
        Map<Integer, User> usersFromFileMap = this.getUsersFromFile();
        return usersFromFileMap.getOrDefault(id, null);
    }

    @Override
    public void update(User user) {
        Map<Integer, User> usersFromFileMap = getUsersFromFile();
        usersFromFileMap.put(user.getUserId(), user);
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(fileName))) {
            StringBuilder text = new StringBuilder();
            usersFromFileMap.forEach((k, v) -> text.append(v.toString()));
            bw.write(String.valueOf(text));
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public Map<Integer, User> getAllUsers() {
        return getUsersFromFile();
    }
}

package hw19reader;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        UsersRepository usersRepository = new UsersRepositoryFileImpl("users.txt");
        List<User> users1 = usersRepository.findAll();
        List<User> users2 = usersRepository.findByAge(27);
        List<User> users3 = usersRepository.findByIsWorkerIsTrue();

        printUser(users1);
        System.out.println();
        printUser(users2);
        System.out.println();
        printUser(users3);

        //User user = new User("Игорь", 33, true);
        //usersRepository.save(user);

    }

    public static void printUser(List<User> users){
        for (User user : users) {
            System.out.println(user.getAge() + " " + user.getName() + " " + user.isWorker());
        }
    }

}

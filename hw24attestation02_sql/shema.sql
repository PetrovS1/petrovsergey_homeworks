CREATE TABLE products
(
    id_product SERIAL PRIMARY KEY ,
    name_product VARCHAR(30),
    price_product REAL,
    count_product INTEGER
);

CREATE TABLE clients
(
    id_client   SERIAL PRIMARY KEY,
    name_client VARCHAR(30)
);

CREATE TABLE orders
(
    id_client INTEGER,
    id_product  INTEGER,
    date_order  DATE,
    count_product  INT,
    FOREIGN KEY (id_client) REFERENCES clients (id_client),
    FOREIGN KEY (id_product) REFERENCES products (id_product)
);

INSERT INTO products (name_product, price_product, count_product) VALUES ('iPhone 13', 100.0, 50);
INSERT INTO products (name_product, price_product, count_product) VALUES ('iPad 9', 80.0, 10);
INSERT INTO products (name_product, price_product, count_product) VALUES ('JBL Charge 5', 11.0, 30);
INSERT INTO products (name_product, price_product, count_product) VALUES ('Asus VivoBook', 75.0, 20);
INSERT INTO products (name_product, price_product, count_product) VALUES ('Xiaomi mi9T', 22.0, 46);
INSERT INTO products (name_product, price_product, count_product) VALUES ('Camry', 800.0, 7);

INSERT INTO clients (name_client) VALUES ('Egor');
INSERT INTO clients (name_client) VALUES ('Albina');
INSERT INTO clients (name_client) VALUES ('Tatiana');
INSERT INTO clients (name_client) VALUES ('Marsel');
INSERT INTO clients (name_client) VALUES ('Rinat');
INSERT INTO clients (name_client) VALUES ('Vladimir');

INSERT INTO orders (id_client, id_product, date_order, count_product) VALUES (1, 4, '2021-12-01', 2);
INSERT INTO orders (id_client, id_product, date_order, count_product) VALUES (1, 5, '2021-12-02', 5);
INSERT INTO orders (id_client, id_product, date_order, count_product) VALUES (2, 1, '2021-12-03', 1);
INSERT INTO orders (id_client, id_product, date_order, count_product) VALUES (2, 3, '2021-12-04', 2);
INSERT INTO orders (id_client, id_product, date_order, count_product) VALUES (4, 1, '2021-12-05', 2);
INSERT INTO orders (id_client, id_product, date_order, count_product) VALUES (4, 1, '2021-12-05', 1);
INSERT INTO orders (id_client, id_product, date_order, count_product) VALUES (2, 4, '2021-12-05', 1);

--какие есть товары:
SELECT name_product FROM products;

--имиена клиентов, которые приобретали что-то 05 декабря 2021 года
SELECT DISTINCT name_client FROM clients
                                     INNER JOIN orders ON clients.id_client = orders.id_client
WHERE date_order = '2021-12-05';

--сумма выручки за 5 декабря 2021 года
SELECT SUM(products.price_product * orders.count_product) FROM orders
                                                                   INNER JOIN products ON products.id_product = orders.id_product
WHERE date_order = '2021-12-05';

--кто и когда покупал iphone 13
SELECT (name_product, date_order, name_client) FROM orders
                                                        LEFT JOIN products ON orders.id_product = products.id_product
                                                        LEFT JOIN  clients ON orders.id_client = clients.id_client
WHERE name_product = 'iPhone 13';

--в какую дату, какой товар и кто купил товары дороже 50 единиц измерения стоимости товара
SELECT (date_order, name_product, name_client) from orders
                                                        LEFT JOIN products ON orders.id_product = products.id_product
                                                        LEFT JOIN clients ON orders.id_client = clients.id_client
where price_product > 50;
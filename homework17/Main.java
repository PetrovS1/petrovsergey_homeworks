package hw17;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

//На вход подается строка с текстом. Необходимо посчитать, сколько встречается раз каждое слово в этой строке.
////Вывести:
//Слово - количество раз
////Использовать Map, string.split(" ") - для деления текста по словам. Слово - символы, ограниченные пробелами справа и слева.
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String string = scanner.nextLine();
        String[] arrWords = string.split(" ");
        Map<String, Integer> mapWords = new HashMap<>();
        for (int i = 0; i < arrWords.length; i++) {
            mapWords.put(arrWords[i], mapWords.getOrDefault(arrWords[i], 0) + 1);
        }
        Set<Map.Entry<String, Integer>> entries = mapWords.entrySet();
        for (Map.Entry<String, Integer> entry : entries) {
            System.out.println("Слово: " + entry.getKey() + " встречается: " + entry.getValue() + " раз");
        }
    }
}
package hw13;

//        В main в качестве condition подставить:
//        - проверку на четность элемента
//        - проверку, является ли сумма цифр элемента четным числом.

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        int[] arr = {145, 12, 555, 46, 28, 812};
        int[] resultArray = Sequence.filter(arr, (n) -> {
            if (isEven(n)) {
                return sumDigits(n);
            } else {
                return false;
            }
        });
       // System.out.println(Arrays.toString(resultArray));
    }

    public static boolean isEven(int value) {
        return value % 2 == 0;
    }

    public static boolean sumDigits(int value) {
        int sumDigits = 0;
        while (value != 0) {
            sumDigits += value % 10;
            value /= 10;
        }
        // System.out.println(sumDigits);
        return isEven(sumDigits);
    }
}

package hw13;

import java.util.Arrays;

public class Sequence {
    /**
     * Данный метод возвращает массив, который содержит элементы, удовлетворяющиие логическому выражению в condition.
     */
    public static int[] filter(int[] array, ByCondition condition) {
        int[] result = new int[array.length];
        int countDigits = 0;
        for (int i = 0; i < array.length; i++) {
            if (condition.isOk(array[i])) {
                result[countDigits] = array[i];
                countDigits++;
            }
        }
        result = Arrays.copyOf(result, countDigits);
        return result;
    }
}

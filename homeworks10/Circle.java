package hw10;

public class Circle extends Ellipse implements Position {
    public Circle(double x, int coordX, int coordY) {
        super(x, x);
        this.coordX = coordX;
        this.coordY = coordY;
    }

    public double getPerimeter() {
        return 2 * Math.PI * x;
    }

    @Override
    public void changePosition(int coordX, int coordY) {
        setCoordX(coordX);
        setCoordY(coordY);
    }


}

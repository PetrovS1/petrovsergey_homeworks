package hw10;

public abstract class Figure {
    protected double x;
    protected double y;
    protected int coordX;
    protected int coordY;

    public double getPerimeter() {
        return 0;
    }

    public Figure(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public void setCoordX(int coordX) {
        this.coordX = coordX;
    }

    public void setCoordY(int coordY) {
        this.coordY = coordY;
    }

    public int getCoordX() {
        return coordX;
    }

    public int getCoordY() {
        return coordY;
    }
}

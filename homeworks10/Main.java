package hw10;

public class Main {
    public static void main(String[] args) {
        Circle circle = new Circle(2, 5, 5);
        Square square = new Square(9, 26, 44);

        Position[] forNewPosition = new Position[2];
        forNewPosition[0] = circle;
        forNewPosition[1] = square;

        System.out.println("Расположение круга по оси Х: " + circle.getCoordX() + ", " + "по оси Y: " + circle.getCoordY());
        System.out.println("Расположение квадрата по оси Х: " + square.getCoordX() + ", " + "по оси Y: " + square.getCoordY());

        for (int i = 0; i < forNewPosition.length; i++) {
            forNewPosition[i].changePosition(100, 100);
        }

        System.out.println("Расположение круга по оси Х: " + circle.getCoordX() + ", " + "по оси Y: " + circle.getCoordY());
        System.out.println("Расположение квадрата по оси Х: " + square.getCoordX() + ", " + "по оси Y: " + square.getCoordY());
    }
}

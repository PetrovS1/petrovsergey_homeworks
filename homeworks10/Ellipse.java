package hw10;

public class Ellipse extends Figure {
    public Ellipse(double x, double y) {
        super(x, y);
    }

    public double getPerimeter() {
        return 4*(Math.PI*x*y+(x-y))/x+y;
    }
}

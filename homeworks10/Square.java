package hw10;

public class Square extends Rectangle implements Position {
    public Square(double x, int coordX, int coordY) {
        super(x, x);
        this.coordX = coordX;
        this.coordY = coordY;
    }
    public double getPerimeter() {
        return 4 * x;
    }

    @Override
    public void changePosition(int coordX, int coordY) {
        setCoordX(coordX);
        setCoordY(coordY);
    }

}

//На вход подается последовательность чисел, оканчивающихся на -1.
//Необходимо вывести число, которе присутствует в последовательности минимальное количество раз.
//Гарантируется:
//Все числа в диапазоне от -100 до 100.
//Числа встречаются не более 2 147 483 647-раз каждое.
//Сложность алгоритма - O(n)

import java.util.*;

public class dz07 {
    public static void main(String[] args) {
        System.out.print("Введите числа: ");
        int[] arrMain = selectionSort(getArray());
        int[] arrCount = findCount(arrMain);
        System.out.println(Arrays.toString(arrMain));
        System.out.println(Arrays.toString(arrCount));
        System.out.println("Числа встречающееся минимальное количество раз: " + getMinDigit(arrMain, arrCount));

    }

    public static int[] getArray() {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        ArrayList<Integer> arr = new ArrayList<>();

        while (a != -1) {
            if (a < -100 || a > 100) {
                System.out.println("Числа должны быть от -100 до 100, запускай заново");
                break;
            }
            arr.add(a);
            a = scanner.nextInt();
        }
        int sizeArr = arr.size();
        Integer[] array = arr.toArray(new Integer[sizeArr]);
        int[] result = Arrays.stream(array).mapToInt(i -> i).toArray();

        return result;
    }

    public static int[] selectionSort(int[] array) {
        for (int i = 0; i < array.length; i++) {
            int min = array[i];
            int minIndex = i;
            for (int j = i + 1; j < array.length; j++) {
                if (array[j] < min) {
                    min = array[j];
                    minIndex = j;
                }
            }
            int temp = array[i];
            array[i] = array[minIndex];
            array[minIndex] = temp;
        }
        return array;
    }

    public static int[] findCount(int[] arr) {
        int[] result = new int[arr.length];
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length; j++) {
                if (arr[i] == arr[j])
                    result[i]++;
            }
        }
        return result;
    }

    public static Set<Integer> getMinDigit(int[] arrMain, int[] arrCount) {
        Set<Integer> treeSet = new TreeSet<>();
        int indexMin = 0;
        int min = arrCount[0];
        for (int i = 0; i < arrMain.length; i++) {
            for (int j = i; j < arrCount.length; j++) {
                if (arrCount[j] <= min) {
                    min = arrCount[j];
                    indexMin = j;
                    treeSet.add(arrMain[indexMin]);
                }
            }
        }
        return treeSet;
    }
}
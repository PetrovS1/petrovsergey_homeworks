package hw09;

public class Main {
    public static void main(String[] args) {
        Figure figure = new Figure(0, 0);
        Circle circle = new Circle(2);
        Rectangle rectangle = new Rectangle(8, 24);
        Square square = new Square(9);
        Ellipse ellipse = new Ellipse(1, 2);

        Figure[] figures = new Figure[5];
        figures[0] = figure;
        figures[1] = circle;
        figures[2] = rectangle;
        figures[3] = square;
        figures[4] = ellipse;

        for (int i = 0; i < figures.length; i++) {
            System.out.println(figures[i].getPerimeter());
        }
    }
}
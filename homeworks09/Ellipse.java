package hw09;

public class Ellipse extends Figure {
    private final double radius1;
    private final double radius2;

    public Ellipse(double radius1, double radius2) {
        super();
        this.radius1 = radius1;
        this.radius2 = radius2;
    }

    public double getRadius1() {
        return radius1;
    }

    public double getPerimeter() {
        return 4*(Math.PI*radius1*radius2+(radius1-radius2))/radius1+radius2;
    }
}
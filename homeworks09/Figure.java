package hw09;

public class Figure {
    protected double coordX;
    protected double coordY;

    public Figure() {
    }

    public double getPerimeter() {
        return 0;
    }

    public Figure(double coordX, double coordY) {
        this.coordX = coordX;
        this.coordY = coordY;
    }
}
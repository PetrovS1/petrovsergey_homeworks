package hw09;

public class Square extends Rectangle {
    public Square(double storonaA) {
        super(storonaA, storonaA);
    }

    public double getPerimeter() {
        return 4 * this.getStoronaA();
    }
}
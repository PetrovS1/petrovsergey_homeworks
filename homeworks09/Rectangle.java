package hw09;

public class Rectangle extends Figure {
    private final double storonaA;
    private final double storonab;

    public Rectangle(double storonaA, double storonab) {
        super();
        this.storonaA = storonaA;
        this.storonab = storonab;
    }

    public double getStoronaA() {
        return storonaA;
    }

    public double getPerimeter() {
        return storonaA * 2 + storonab * 2;
    }
}